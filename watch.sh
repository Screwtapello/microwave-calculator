cd "$(dirname "$0")"

# Build once, to collect the list of source files.
redo

# Rebuild whenever a source file changes
redo-sources | entr redo
