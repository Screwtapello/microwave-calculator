redo-ifchange appname.txt

APPNAME=$(cat appname.txt)

cat << EOF
    <!DOCTYPE HTML>
    <html>

    <head>
      <meta charset="UTF-8">
      <title>Microwave Calculator</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
      <link rel="manifest" href="manifest.json">
      <link rel="icon" href="icon.png">
      <link rel="stylesheet" href="spectre.min.css">
      <script type="text/javascript" src="elm.js"></script>
      <style type="text/css">
          article { width: 90%; max-width: 900px; margin: 0 auto; min-height: 100vh; }
      </style>
    </head>

    <body></body>

    <script type="text/javascript">
EOF

if [ "$2" = "prod/index" ]; then cat << "EOF"
    // Install our service worker so we can work offline.
    if('serviceWorker' in navigator) {
        navigator.serviceWorker.register('service-worker.js');
    };
EOF
fi

cat << EOF
    // Extract the stored data from previous sessions.
    var storedData = localStorage.getItem('$APPNAME-model');
    var flags = storedData ? JSON.parse(storedData) : null;

    // Load the Elm app, passing in the stored data.
    var app = Elm.Main.init({ flags: flags });

    // Listen for commands from the setStorage port.
    // Turn the data to a string and put it in localStorage.
    app.ports.setStorage.subscribe(function(state) {
        localStorage.setItem('$APPNAME-model', JSON.stringify(state));
    });
    </script>

    </html>
EOF
