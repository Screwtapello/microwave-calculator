redo-ifchange \
    ../elm/elm-prod.js \
    prod/index.html \
    service-worker.js \
    resource-list.txt
redo-ifchange $(cat resource-list.txt)

cp -a ../elm/elm-prod.js prod/elm.js
cp -a service-worker.js $(cat resource-list.txt) prod/
