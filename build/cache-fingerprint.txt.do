redo-ifchange ../elm/elm-prod.js resource-list.txt
redo-ifchange $(cat resource-list.txt)

sha256sum ../elm/elm-prod.js $(cat resource-list.txt) |
    LANG=C sort |
    sha256sum |
    cut -d" " -f1
