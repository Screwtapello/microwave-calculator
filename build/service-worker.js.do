redo-ifchange resource-list.txt cache-fingerprint.txt appname.txt

APPNAME=$(cat appname.txt)
FINGERPRINT=$(cat cache-fingerprint.txt)

# Offline-caching service worker skeleton from
# https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Offline_Service_workers

cat << EOF
    const cacheName = '$APPNAME-$FINGERPRINT';
    const appFiles = [
EOF

cat resource-list.txt | while read file; do
    printf "        '%s',\n" "${file#../*/}"
done

cat << EOF
        '.',
        'elm.js'
    ];

    self.addEventListener('install', (e) => {
        console.log('[Service Worker] Install');
        e.waitUntil((async () => {
            const cache = await caches.open(cacheName);
            console.log('[Service Worker] Caching all app files');
            await cache.addAll(appFiles);
        })());
    });

    self.addEventListener('activate', (e) => {
        e.waitUntil(caches.keys().then((keyList) => {
            return Promise.all(keyList.map((key) => {
                console.log('[Service Worker] Checking cache ' + key);
                // Don't delete our current cache!
                if (key === cacheName) {
                    console.log('[Service Worker] This is us!');
                    return;
                }
                // Don't delete caches for other apps in the same origin.
                if (!key.startsWith('$APPNAME-')) {
                    console.log('[Service Worker] This is not ours!');
                    return;
                }
                console.log('[Service Worker] Clearing cache ' + key);
                return caches.delete(key);
            }))
        }));
    });

    self.addEventListener('fetch', (e) => {
      e.respondWith((async () => {
        const r = await caches.match(e.request);
        console.log('[Service Worker] Fetching resource: ' + e.request.url);
        if (r) {
            return r;
        } else {
            console.error('[Service Worker] Resource not cached!')
            return await fetch(e.request);
        }
      })());
    });
EOF
