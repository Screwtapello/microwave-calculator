redo-ifchange elm.json src/*.elm uglify-wrapper

# elm looks at the file extension to guess what format to write,
# and redo's standard temp file doesn't fit the bill.
TEMPFILE=$(mktemp "$PWD"/XXXXXXXX.js)
trap "rm -f $TEMPFILE" EXIT

elm make --optimize --output "$TEMPFILE" src/Main.elm >&2

cat "$TEMPFILE" |
    ./uglify-wrapper --compress "pure_funcs=[F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9],pure_getters,keep_fargs=false,unsafe_comps,unsafe" |
    ./uglify-wrapper --mangle >"$3"
