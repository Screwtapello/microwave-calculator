port module Main exposing (Model, Msg(..), decoder, encode, formatTime, init, main, parseTime, setStorage, update, view)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as D
import Json.Encode as E


parseTime : String -> Float
parseTime value =
    let
        maybe_fields =
            String.split ":" value
                |> List.map String.toFloat
                |> List.map (Maybe.withDefault 0.0)
    in
    case maybe_fields of
        -- For a single field, we use it as-is
        [ minutes ] ->
            minutes

        -- For any other number of fields,
        -- we assume the last field is seconds, and we want minutes.
        fields ->
            List.foldl (\a b -> b * 60.0 + a) 0.0 fields / 60


formatTime : Float -> String
formatTime value =
    let
        minutes =
            truncate value

        seconds =
            round ((value - toFloat minutes) * 60)
    in
    String.concat
        [ String.fromInt minutes
        , ":"
        , String.padLeft 2 '0' (String.fromInt seconds)
        ]



-- MAIN


main : Program E.Value Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }



-- MODEL


type alias Model =
    { listed_strength : Float
    , listed_time_raw : String
    , listed_time : Float
    , your_strength : Float
    }


decoder : D.Decoder Model
decoder =
    D.map4 Model
        (D.field "listed_strength" D.float)
        (D.field "listed_time_raw" D.string)
        (D.field "listed_time" D.float)
        (D.field "your_strength" D.float)


encode : Model -> E.Value
encode model =
    E.object
        [ ( "listed_strength", E.float model.listed_strength )
        , ( "listed_time_raw", E.string model.listed_time_raw )
        , ( "listed_time", E.float model.listed_time )
        , ( "your_strength", E.float model.your_strength )
        ]


init : E.Value -> ( Model, Cmd Msg )
init flags =
    ( case D.decodeValue decoder flags of
        Ok m ->
            m

        Err _ ->
            { listed_strength = 1000.0
            , listed_time_raw = "7:00"
            , listed_time = 7.0
            , your_strength = 700.0
            }
    , Cmd.none
    )



-- UPDATE


type Msg
    = SetListedStrength String
    | SetListedTimeRaw String
    | ProcessListedTimeRaw
    | SetYourStrength String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetListedStrength value ->
            let
                strength =
                    String.toFloat value
                        |> Maybe.withDefault model.listed_strength

                new_model =
                    { model | listed_strength = strength }
            in
            ( new_model, setStorage (encode new_model) )

        SetListedTimeRaw value ->
            let
                new_model =
                    { model | listed_time_raw = value }
            in
            ( new_model, setStorage (encode new_model) )

        ProcessListedTimeRaw ->
            let
                listed_time =
                    parseTime model.listed_time_raw

                listed_time_raw =
                    formatTime listed_time

                new_model =
                    { model
                        | listed_time = listed_time
                        , listed_time_raw = listed_time_raw
                    }
            in
            ( new_model, setStorage (encode new_model) )

        SetYourStrength value ->
            let
                strength =
                    String.toFloat value
                        |> Maybe.withDefault model.your_strength

                new_model =
                    { model | your_strength = strength }
            in
            ( new_model, setStorage (encode new_model) )



-- VIEW


view : Model -> Browser.Document Msg
view model =
    Browser.Document
        "Microwave Calculator"
        [ header [ class "navbar" ]
            [ section [ class "navbar-section" ]
                [ span [ class "navbar-brand mr-2" ]
                    [ text "Microwave Calculator" ]
                ]
            , section [ class "navbar-section" ]
                [ a
                    [ class "navbar-brand mr-2"
                    , href "https://gitlab.com/Screwtapello/microwave-calculator"
                    ]
                    [ text "Source Code" ]
                ]
            ]
        , article []
            [ Html.form []
                [ div [ class "form-group" ]
                    [ label [ class "form-label", for "listed_strength" ]
                        [ text "Listed Microwave Strength (Watts)" ]
                    , input
                        [ class "form-input"
                        , type_ "number"
                        , id "listed_strength"
                        , value (String.fromFloat model.listed_strength)
                        , onInput SetListedStrength
                        ]
                        []
                    ]
                , div [ class "form-group" ]
                    [ label [ class "form-label", for "your_strength" ]
                        [ text "Your Microwave's Strength (Watts)" ]
                    , input
                        [ class "form-input"
                        , type_ "number"
                        , id "your_strength"
                        , value (String.fromFloat model.your_strength)
                        , onInput SetYourStrength
                        ]
                        []
                    ]
                , div [ class "form-group" ]
                    [ label [ class "form-label", for "listed_time" ]
                        [ text "Listed Cooking Time" ]
                    , input
                        [ class "form-input"
                        , type_ "text"
                        , attribute "inputmode" "decimal"
                        , pattern "\\d+|\\d+\\.|\\.\\d+|\\d+\\.\\d+|\\d+:\\d+"
                        , placeholder "7:30 or 7.5"
                        , id "listed_time"
                        , value model.listed_time_raw
                        , onInput SetListedTimeRaw
                        , onBlur ProcessListedTimeRaw
                        ]
                        []
                    ]
                , div [ class "form-group" ]
                    [ label [ class "form-label", for "your_time" ]
                        [ text "Your Microwave's Time" ]
                    , input
                        [ class "form-input"
                        , type_ "text"
                        , id "your_time"
                        , readonly True
                        , value (formatTime (model.listed_strength / model.your_strength * model.listed_time))
                        ]
                        []
                    ]
                ]
            ]
        ]



-- PORTS


port setStorage : E.Value -> Cmd msg
