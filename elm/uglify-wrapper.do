redo-ifchange path-list find_executable.od

. ./find_executable.od

NAMES="terser uglifyjs.terser uglifyjs"

for name in $NAMES; do
    UGLIFY=$(find_executable "$name")
    if [ -n "$UGLIFY" ]; then break; fi
done

printf '#!/bin/sh\n' >> "$3"

if [ -n "$UGLIFY" ]; then
    printf 'exec %s "$@"\n' "$UGLIFY" >>"$3"
else
    printf 'echo Could not find uglifyjs, building unminified >&2\n' >>"$3"
    printf 'exec cat\n' >>"$3"
fi

chmod 755 "$3"

redo-stamp <"$3"
