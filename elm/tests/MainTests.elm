module MainTests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, floatRange, triple)
import Json.Decode as D
import Json.Encode as E
import Main
import Test exposing (..)


-- We want to fuzz our time parsing/formatting code,
-- but we don't expect to handle crazy floats
-- outside the range of integers
sensibleFloat : Fuzzer Float
sensibleFloat = floatRange 0.0 1000000.0



modelFuzzer : Fuzzer Main.Model
modelFuzzer =
    triple sensibleFloat sensibleFloat sensibleFloat
        |> Fuzz.map
            (\( listed_strength, listed_time, your_strength ) ->
                { listed_strength = listed_strength
                , listed_time = listed_time
                , listed_time_raw = Main.formatTime listed_time
                , your_strength = your_strength
                }
            )


suite : Test
suite =
    describe "The Main module"
        [ describe "parseTime"
            [ test "parses an empty string as 0" <|
                \_ ->
                    Main.parseTime ""
                        |> Expect.equal 0.0
            , test "parses an integer" <|
                \_ ->
                    Main.parseTime "1"
                        |> Expect.equal 1.0
            , test "parses an integer with leading zeroes" <|
                \_ ->
                    Main.parseTime "08"
                        |> Expect.equal 8.0
            , describe "parses time notation"
                [ test "with empty seconds" <|
                    \_ ->
                        Main.parseTime "1:"
                            |> Expect.equal 1.0
                , test "with one-digit seconds" <|
                    \_ ->
                        Main.parseTime "1:0"
                            |> Expect.equal 1.0
                , test "with two-digit seconds" <|
                    \_ ->
                        Main.parseTime "1:00"
                            |> Expect.equal 1.0
                , test "Parse 1:30 as 1.5 minutes" <|
                    \_ ->
                        Main.parseTime "1:30"
                            |> Expect.within (Expect.Absolute 0.0001) 1.5
                , test "Parse 1:00:0 as 1 hour" <|
                    \_ ->
                        Main.parseTime "1:00:0"
                            |> Expect.equal 60.0
                , test "Parse 1:30:00 as 1.5 hours" <|
                    \_ ->
                        Main.parseTime "1:30:00"
                            |> Expect.equal 90.0
                , test "Parse 1:30:30 as 90.5 minutes" <|
                    \_ ->
                        Main.parseTime "1:30:30"
                            |> Expect.within (Expect.Absolute 0.0001) 90.5
                ]
            , describe "handles decimal notation"
                [ test "Parse 1.0 as 1 minute" <|
                    \_ ->
                        Main.parseTime "1.0"
                            |> Expect.equal 1.0
                , test "Parse 1.5 as 1.5 minutes" <|
                    \_ ->
                        Main.parseTime "1.5"
                            |> Expect.within (Expect.Absolute 0.0001) 1.5
                ]
            , describe "handles mixed time and decimal notation"
                [ test "Parse 1:00.5 as 1 minute" <|
                    \_ ->
                        Main.parseTime "1:00.6"
                            |> Expect.within (Expect.Absolute 0.0001) 1.01
                ]
            ]
        , describe "formatTime"
            [ test "formats 0" <|
                \_ ->
                    Main.formatTime 0
                        |> Expect.equal "0:00"
            , test "formats 0.5 as 30 seconds" <|
                \_ ->
                    Main.formatTime 0.5
                        |> Expect.equal "0:30"
            ]
        , fuzz sensibleFloat "formatTime and parseTime are inverse of each other" <|
            \x ->
                Main.formatTime x
                    |> Main.parseTime
                    |> Expect.within (Expect.Absolute 0.1) x
        , fuzz modelFuzzer "encode and decode are inverse of each other" <|
            \x ->
                Main.encode x
                    |> E.encode 0
                    |> D.decodeString Main.decoder
                    |> Expect.equal (Ok x)
        ]
