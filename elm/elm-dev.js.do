redo-ifchange elm.json src/*.elm

# elm looks at the file extension to guess what format to write,
# and redo's standard temp file doesn't fit the bill.
TEMPFILE=$(mktemp "$PWD"/XXXXXXXX.js)
trap "rm -f $TEMPFILE" EXIT

elm make --output "$TEMPFILE" src/Main.elm >&2

mv "$TEMPFILE" "$3"
