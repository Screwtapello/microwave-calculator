redo-ifchange path-list find_executable.od

. ./find_executable.od

NAMES="elm-test elm-test-rs"

for name in $NAMES; do
    TESTER=$(find_executable "$name")
    if [ -n "$TESTER" ]; then break; fi
done

printf '#!/bin/sh\n' >> "$3"

if [ -n "$TESTER" ]; then
    printf 'exec %s "$@"\n' "$TESTER" >>"$3"
else
    printf 'echo Could not find elm-test, cannot run tests >&2\n' >>"$3"
fi

chmod 755 "$3"

redo-stamp <"$3"
